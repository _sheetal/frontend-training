import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { MockData } from '../mock-data';
@Injectable({
  providedIn: 'root'
})
export class TableServiceService {

  constructor(private http:HttpClient) { }

  public fetch():Observable<any>{
    return this.http.get("https://jsonplaceholder.typicode.com/posts");
  }
}

