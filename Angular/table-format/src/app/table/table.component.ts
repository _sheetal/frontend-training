import { Component, OnInit } from '@angular/core';
import { MockData } from '../mock-data';
import { TableServiceService } from '../services/table-service.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

   datas:MockData[]=[];
// datas={
//   userid:'',
//     id:'',
//     title:'',
//     body:''
// }
  constructor(private service:TableServiceService) { }

  ngOnInit(): void {
    this.service.fetch().subscribe(
      data=>this.datas=data,err=>console.log("cannot fetched")
    )
  }

}
