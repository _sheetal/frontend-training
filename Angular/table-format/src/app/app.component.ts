import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public loadComponent=false;
  title = 'table-format';
  loadChildComponent()
  {
    this.loadComponent=true
  }
}
