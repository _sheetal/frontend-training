import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RadioComponent } from './radio/radio.component';
import { TableComponent } from './table/table.component';
const routes: Routes = [
  {
    path:'table',component:TableComponent
  },
  {
    path:'radio',component:RadioComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
