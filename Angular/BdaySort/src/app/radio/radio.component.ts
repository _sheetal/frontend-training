import { Component, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
@Component({
  selector: 'app-radio',
  templateUrl: './radio.component.html',
  styleUrls: ['./radio.component.css']
})
export class RadioComponent implements OnInit {

  @Output() sortName  = new EventEmitter();
  // outputText : string = "Hi ... message from child";
  @Output() sortAge  = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }
  sortByAgeSend()
  {
    this.sortAge.emit()
  }
  sortByNameSend()
  {
    this.sortName.emit()
  }

}
