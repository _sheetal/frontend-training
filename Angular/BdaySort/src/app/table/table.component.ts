import { Component, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { SampleService } from '../services/sample.service';
import { User } from '../user';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

  Datas:User[]=[];

  @Output()
  sendToParent: EventEmitter<User[]>=new EventEmitter<User[]>()
  constructor(private service:SampleService) { }

  ngOnInit(): void {
    this.service.fetch().subscribe(
      data=>this.Datas=data,
      // data=>this.sendToParent.emit(data),
      err=>console.log("pls check error occurs"))
      
  }


  sortByAge()
  {
    this.Datas=this.Datas.sort((a,b)=>{
      if(a.age > b.age)
      {
        return 1;
      }
      if(a.age < b.age)
      {
        return -1;
      }
      else
      {
        return 0;
      }
    });
  }

  sortByName()
  {
    this.Datas=this.Datas.sort((a,b)=>{
      if(a.name > b.name)
      {
        return 1;
      }
      if(a.name < b.name)
      {
        return -1;
      }
      else
      {
        return 0;
      }
    });
  }

}
