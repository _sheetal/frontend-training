import { Component, OnInit } from '@angular/core';
// import swal from 'sweetalert';

@Component({
  selector: 'app-convert',
  templateUrl: './convert.component.html',
  styleUrls: ['./convert.component.css']
})
export class ConvertComponent implements OnInit {

  inp={
    amount:'',
    from:'',
    to:''
  }

  // currency={
  //   {cu:}
  // }
  constructor() { }

  ngOnInit(): void {
  }

  res=0;
  onSubmit()
  {
      let amt=this.inp.amount;
      var from=this.inp.from;
      var to=this.inp.to;
      if(from=="USD" && to=="INR")
      {
        this.res=parseInt(amt)*74.5;
      }
      else if(from=="INR" && to=="USD")
      {
        this.res=parseInt(amt)*0.013;
      }
      
      // swal("res");
  }
}
