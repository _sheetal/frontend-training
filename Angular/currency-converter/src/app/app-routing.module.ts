import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConvertComponent } from './convert/convert.component';

const routes: Routes = [
  {path:'',redirectTo:"/currency", pathMatch: 'full'},
  {path:"currency",component:ConvertComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
