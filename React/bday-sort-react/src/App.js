import user from './user.json'
import './App.css';
import Table from './Table'
import { useEffect, useState } from 'react';
import Radio from './Radio';
function App() {

  const [table,setTable]=useState([]);

  useEffect(()=>{
    // main api call
    // ComponentDidMount
    console.log("run")
    fetchData();
}, [])

  const fetchData=()=>{
    fetch("http://localhost:3000/user.json",{
      header:{
          'Content-Type':'application/json',
          'Accept':'application/json'
    }})
    .then(res => {
        return res.json()
    })
    .then(data => {
        
        console.log(data)
        setTable(data);
        
    })
    .catch(err => {
        console.log('Error ---- ', err)
    })
  }

  const sortByAge=()=>{
    const sorted = [...table].sort((a,b)=>{
        var n=a.age;
        var m=b.age
        if(n<m)
        {
          return -1;
        }
        if(n>m)
        {
          return 1;
        }
        return 0;
    });
    setTable(sorted);
  }

  const sortByName=()=>{
    const sorted = [...table].sort((a,b)=>{
        var n=a.name.toUpperCase();
        var m=b.name.toUpperCase();
        if(n<m)
        {
          return -1;
        }
        if(n>m)
        {
          return 1;
        }
        return 0;
    });
    setTable(sorted);
  }
 

  return (
    <div className="App">
      <Table table={table} />
      <Radio sortByName={sortByName} sortByAge={sortByAge} />
    </div>
  );
}

export default App;
