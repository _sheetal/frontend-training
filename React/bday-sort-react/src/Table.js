import {React} from 'react';
const Table = ({table}) => {

    //console.log(table[0].name);
    const display=table.map((data)=>{
        return(
            <tr key={data.id}>
                <td>{data.id}</td>
                <td>{data.name}</td>
                <td>{data.age}</td>
            </tr>
        )
    })

    return ( 
        
        <div>
            
            <table>
                <thead>
                    <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Age</th>
                    </tr>
                </thead>
                <tbody>
                    {/* {display} */}
                    {table.map((data) => {
                    return (
                    <tr key={data.id}>
                        <td>{data.id}</td>
                        <td>{data.name}</td>
                        <td>{data.age}</td>
              
                    </tr>
                    )
                    })}
                </tbody>
            </table>
        </div>
    );
}
 
export default Table;