import logo from './logo.svg';
import './App.css';
import {useState} from 'react';
import Table from './Table';
import Detail from './Detail';
import {BrowserRouter as Router,Switch,Route,Link} from 'react-router-dom';
function App() {
  const [table,setTable]=useState([]);

  const getPosts=()=>{
    fetch('https://jsonplaceholder.typicode.com/posts')
    .then(res => {
        return res.json()
    })
    .then(data => {
        //
        
        setTable(data);
        
    })
    .catch(err => {
        console.log('Error ---- ', err)
    })
}



  return (
    
    <div className="App">
      
      <button onClick={getPosts}>Click me</button>
      {
        table.map((data,key)=>{
          return(
            <div key={key}>
              <Table
                table={table} />
            </div>
          );
        })
      }
    </div>
  );

  }
export default App;
