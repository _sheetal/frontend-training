import { useState } from "react";
import { useHistory } from "react-router-dom";


//  const Table = ({id,title}) => {
const Table = ({table}) => {
    
    const his=useHistory();
    //const [cur,setCur]=useState();

    

    const display=table.map((data)=>{
        return(
            <tr key={data.id}>
                <td>{data.id}</td>
                <td>{data.title}</td>
                <td>{data.body}</td>
            </tr>
        )
    })
    
    // if(!id) return<div />;
    return ( 
        
        <div>
            <table>
                <thead>
                    <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Body</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        {display}
                    </tr>
                </tbody>
            </table>
        </div>
     );
}
 
export default Table;